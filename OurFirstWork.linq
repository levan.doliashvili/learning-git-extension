<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Management.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.Install.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\Microsoft.JScript.dll</Reference>
  <Namespace>System.Dynamic</Namespace>
  <Namespace>System.Management</Namespace>
</Query>

void Main()
{
	var processes = Process.GetProcesses().Select(p => new { p.ProcessName, p.Id});//.Dump();
	
	foreach (var process in processes)
	{
		var cPUandRam = GetUsage(process.ProcessName, process.Id);

		$" ProcessName: {process.ProcessName} ProcessId: {process.Id} UserName: {Process.GetProcessById(process.Id).StartInfo.UserName} Memory Usage: {process} CPU: {cPUandRam.CPU}".Dump();
	}
}


public static SmartDouble GetUsage(string processName, int processIdP)
{
	// Getting information about current process
	//var process = proc;
	// Preparing variable for application instance name
	var name = string.Empty;
	var instances = new PerformanceCounterCategory("Process").GetInstanceNames();
	foreach (var instance in instances)
	{
		if (instance.StartsWith(processName))
		{
			using (var processId = new PerformanceCounter("Process", "ID Process", instance, true))
			{
				if (processIdP == (int)processId.RawValue)
				{
					name = instance;
					break;
				}
			}
		}
	}
	var cpu = new PerformanceCounter("Process", "% Processor Time", name, true);
	var ram = new PerformanceCounter("Process", "Private Bytes", name, true);
	// Getting first initial values
	cpu.NextValue();
	ram.NextValue();
	// Creating delay to get correct values of CPU usage during next query
	Thread.Sleep(500);
	var result= new SmartDouble();
	// If system has multiple cores, that should be taken into account
	result.CPU = Math.Round(cpu.NextValue() / Environment.ProcessorCount, 2);
	// Returns number of MB consumed by application
	result.RAM = Math.Round(ram.NextValue() / 1024 / 1024, 2);
	return result;
}

public class SmartDouble
{
	public double CPU { get; set; }
    public double RAM { get; set; }
}